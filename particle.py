import matplotlib.pyplot as plt
from datetime import datetime

_k = 8.987551e9 #coulomb cte
_e = 1.6e-19  #elementary electric charge
_mp = 1.67e-27 #proton mass

class particle():
  def __init__(self,pos,vel,**kwargs):
      '''
      Esta clase inicializa la partícula con los siguientes atributos:
      pos: Posición inicial de la partícula
      vel: Velocidad inicial de la partícula
      '''
      self.pos = pos 
      self.vel = vel
      self.acel = [0,0,0]

      self.mass = kwargs.get("mass",_mp)
      self.charge = kwargs.get("charge",_e)
  
  def get_distance_sr(self,point):
      '''
      Calcula la distancia al cuadrado entre esta partícula y un punto x,y,z del espacio
      point: lista del punto del espacio en R3
      '''
      x,y,z = point
      x0,y0,z0 = self.pos

      return (x-x0)**2+(y-y0)**2+(z-z0)**2

  def _get_coulomb(self,particle,k = _k):
      '''
      Calcula la interacción de coulomb entre esta instancia y otra partícula
      particle: una instancia de la clase particle
      k: Constante de coulomb
      '''
      q1 = self.charge
      q2 = particle.charge
      r_squared = self.get_distance_sr(particle.pos)

      unitx = (particle.pos[0]-self.pos[0])/r_squared**(1/2) #vector unitario en x
      unity = (particle.pos[1]-self.pos[1])/r_squared**(1/2) #vector unitario en y
      unitz = (particle.pos[2]-self.pos[2])/r_squared**(1/2) #vector unitario en z
      
      fcx=k*q1*q2*unitx/r_squared #Componente x de la fuerza de coulomb 
      fcy=k*q1*q2*unity/r_squared #Componente y de la fuerza de coulomb
      fcz=k*q1*q2*unitz/r_squared #Componente z de la fuerza de coulomb

      return [fcx,fcy,fcz]

  def _get_lorentz(self,field):
      '''
      Calcula la interacción de Lorentz entre esta instancia y un campo
      field: Una instancia de la clase campo
      '''
      q  = self.charge
      vx = self.vel[0]
      vy = self.vel[1]
      vz = self.vel[2]
      Bx = field.x
      By = field.y
      Bz = field.z
      
      flx=q*(vy*Bz-vz*By) #componente x de la fuerza de lorentz
      fly=q*(vz*Bx-vx*Bz) #componente y de la fuerza de lorentz
      flz=q*(vx*By-vy*Bx) #componente z de la fuerza de lorentz

      return [flx,fly,flz] 

  def _update_acel(self,particle = None,B = None):
      '''
      Actualiza la aceleración despejando F = ma
      '''
      m = self.mass

      if particle:
        fcx, fcy, fcz = self._get_coulomb(particle)
      else:
        fcx, fcy, fcz = [0,0,0]

      if B:
        flx, fly, flz = self._get_lorentz(B)
      else:
        flx, fly, flz = [0,0,0]
#El signo negativo es debido a la definición de los vectores unitarios abajo
      acelx = (flx-fcx)/m  #Despejando la componente de la aceleración en x por la segunda ley de newton
      acely = (fly-fcy)/m  #Despejando la componente de la aceleración en y por la segunda ley de newton
      acelz = (flz-fcz)/m  #Despejando la componente de la aceleración en z por la segunda ley de newton

      self.acel = [acelx,acely,acelz]
  
  def _update_vel(self,t):
      '''
      Actualiza la velocidad usando v=v_0+0.5a*t**2
      '''
      self.vel = [self.vel[i]+self.acel[i]*t for i in range(0,3)]
  
  def _update_pos(self,t):
      '''
      Actualiza la posición usando x=x_0+v*t+0.5a*t**2
      '''
      self.pos = [self.pos[i]+self.vel[i]*t+0.5*self.acel[i]*t**2 for i in range(0,3)]
      

class field():
  def __init__(self,*args):
      '''
      Una clase simple para un campo simple
      '''
      self.x, self.y, self.z = args

class simulate():

  def __init__(self,particle1,particle2,field,T,N = 10000, epsilon = 0.00005):
    ''' 
    Esta clase contiene los métodos para la simulación del movimiento para dos particulas y un campo
    particle[1-2]: una instancia de la clase particle
    field: una instancia de la clase field
    T: Tiempo de la simulación
    N: {int} número de datos de la simulación.
    epsilon: {float} magnitud mínima de la distancia entre las partículas al cuadrado (punto de choque), existe para evitar que las gráficas diverjan cuando las partículas están muy cerca.
    
    '''
    self.time = [0] #inicialización del array de tiempo
    dt = T/N  #intervalo infinitesimal de tiempo

    self.pos1 = [particle1.pos] #posición inicial de la partícula 1
    self.pos2 = [particle2.pos] #posición inicial de la partícula 2

    self.vel1 = [particle1.vel] #velocidad inicial de la partícula 1
    self.vel2 = [particle2.vel] #velocidad inicial de la partícula 2

    self.acel1 = [particle1.acel] #aceleración incial de la partícula 1
    self.acel2 = [particle2.acel] #aceleración incial de la partícula 2

    self._save_data(particle1,particle2,field,T,dt,epsilon) #Función de abajo

  def _save_data(self,particle1,particle2,field,T,dt,epsilon):
    ''' 
    Esta función crea los array de los datos de posición, velocidad y aceleración de cada partícula en cada instante de tiempo

    '''

    while self.time[-1]<T:
      self._update_particles(particle1,particle2,field,dt)

      if particle1.get_distance_sr(particle2.pos)< epsilon: #Para evitar las divergencias en los gráficos
        break

      self.pos1 += [particle1.pos] 
      self.pos2 += [particle2.pos] 

      self.vel1 += [particle1.vel] 
      self.vel2 += [particle2.vel] 

      self.acel1 += [particle1.acel] 
      self.acel2 += [particle2.acel] 

      self.time += [self.time[-1]+dt] 
      
  
  def _update_particles(self,particle1,particle2,field,dt):
    ''' 
    Esta función actualiza el estado dinámico de las partículas usando las ecuaciones de movimiento
    '''
    particle1._update_acel(particle2,field) #Actualiza la aceleración de la partícula 1
    particle2._update_acel(particle1,field) #Actualiza la aceleración de la partícula 2

    particle1._update_vel(dt) #Actualiza la velocidad de la partícula 1
    particle2._update_vel(dt) #Actualiza la velocidad de la partícula 2
    
    particle1._update_pos(dt) #Actualiza la posición de la partícula 1
    particle2._update_pos(dt) #Actualiza la posición de la partícula 2
  

  def show_pos(self,*args,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las componentes de la posición de cada partícula, si no se entrega ningún argumento muestra las 3 componentes en un subplot, si se pasa un argumento de la forma simulate.show_pos("x") o simulate.show_pos("x","z") muestra sólo las componentes "x" o "x" y "z" de la posición

    '''
    
    #Definitivamente hay una mejor forma de hacer todo esto.
    dict = {"x":0,"y":1,"z":2}
    pos1_trans = list(zip(*self.pos1)) #transpone los arrays con las posiciones para ser más fácil de trabajar
    pos2_trans = list(zip(*self.pos2))

    keys = []
    for key in list(set(args).intersection(dict.keys())):
      keys += [key]

    if len(keys)==0:
      keys = ['x','y','z']
      fig, axes = plt.subplots(1,3,figsize = (13,5),dpi = 150)
      for key,ax in zip(keys,axes):
        ax.plot(self.time,pos1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,pos2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Posición en {key}')
    else:
      print(keys)
      fig, axes = plt.subplots(1,len(keys),figsize = (13,5),dpi = 150)
      if len(keys)==1:
        axes = [axes]
      for key,ax in zip(keys,axes):
        ax.plot(self.time,pos1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,pos2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylim(-10,10)
        ax.set_ylabel(f'{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Posición en {key}')


    plt.legend()
    plt.tight_layout()

    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Position_{str}.png',bbox_inches ='tight')



  def show_vel(self,*args,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las componentes de la velocidad de cada partícula, si no se entrega ningún argumento muestra las 3 componentes en un subplot, si se pasa un argumento de la forma simulate.show_vel("x") o simulate.show_vel("x","z") muestra sólo las componentes "x" o "x" y "z" de la velocidad.

    '''
    
    dict = {"x":0,"y":1,"z":2}
    vel1_trans = list(zip(*self.vel1)) #transpone los arrays con las velocidades para ser más fácil de trabajar
    vel2_trans = list(zip(*self.vel2))

    keys = []

    for key in list(set(args).intersection(dict.keys())):
      keys += [key]

    if len(keys)==0:
      keys = ['x','y','z']
      fig, axes = plt.subplots(1,3,figsize = (13,5),dpi = 150)
      for key,ax in zip(keys,axes):
        ax.plot(self.time,vel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,vel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'V_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Velocidad en {key}')

    else:
      fig, axes = plt.subplots(1,len(keys),figsize = (13,5),dpi = 150)
      if len(keys)==1:
        axes = [axes]
      for key,ax in zip(keys,axes):
        ax.plot(self.time,vel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,vel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'V_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Velocidad en {key}')


    plt.legend()
    plt.tight_layout()

    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Velocities_{str}.png',bbox_inches ='tight')


  def show_acel(self,*args,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las componentes de la aceleración de cada partícula, si no se entrega ningún argumento muestra las 3 componentes en un subplot, si se pasa un argumento de la forma simulate.show_acel("x") o simulate.show_acel("x","z") muestra sólo las componentes "x" o "x" y "z" de la velocidad.

    '''
    
    dict = {"x":0,"y":1,"z":2}
    acel1_trans = list(zip(*self.acel1)) #transpone los arrays con las aceleraciones para ser más fácil de trabajar
    acel2_trans = list(zip(*self.acel2))

    keys = []

    for key in list(set(args).intersection(dict.keys())):
      keys += [key]

    if len(keys)==0:
      keys = ['x','y','z']
      fig, axes = plt.subplots(1,3,figsize = (13,5),dpi = 150)
      for key,ax in zip(keys,axes):
        ax.plot(self.time,acel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,acel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'a_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Aceleración en {key}')

    else:
      fig, axes = plt.subplots(1,len(keys),figsize = (13,5),dpi = 150)
      if len(keys)==1:
        axes = [axes]
      for key,ax in zip(keys,axes):
        ax.plot(self.time,acel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,acel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'a_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Aceleración en {key}')

    plt.legend()
    plt.tight_layout()

    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Aceleration_{str}.png',bbox_inches ='tight')

  def show_traject(self,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las trayectorias de ambas partículas en el espacio R3
    '''

    fig = plt.figure(figsize = (13,5),dpi = 150)
    ax = fig.add_subplot(111,projection='3d')

    ax.plot(*list(zip(*self.pos1)),"k", label='Partícula 1',**kwargs)
    ax.plot(*list(zip(*self.pos2)),"b",label='Partícula 2',**kwargs)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_title('Trayectoria de las partículas')

    plt.legend()
    plt.tight_layout()
    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Trajectory_{str}.png',bbox_inches ='tight')
    